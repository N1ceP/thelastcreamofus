import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class Zeze implements KeyboardHandler {


    private Keyboard keyboard;

    public static final int  ZEZE_SIZE = 87;

    private Field field;

    private Picture zezePicture;

    private int height = 87;

    private int width = 29;


    public Zeze(Field field) {
        int startX = (Field.WIDTH - ZEZE_SIZE) /2;
        int startY = Field.HEIGHT - 400;


        zezePicture = new Picture(startX, startY, "zeze2.png");
        zezePicture.draw();
        keyboardInit();

        this.field = field;


    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public int getX(){
        return zezePicture.getX();
    }

    public  int getY(){
        return zezePicture.getY();
    }

    private void keyboardInit() {

        keyboard = new Keyboard(this);

        KeyboardEvent moveRight = new KeyboardEvent();
        moveRight.setKey(39);
        moveRight.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(moveRight);

        KeyboardEvent moveLeft = new KeyboardEvent();
        moveLeft.setKey(37);
        moveLeft.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(moveLeft);

        KeyboardEvent moveUp = new KeyboardEvent();
        moveUp.setKey(38);
        moveUp.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(moveUp);

        KeyboardEvent moveDown = new KeyboardEvent();
        moveDown.setKey(40);
        moveDown.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(moveDown);
    }

    public void moveRight() {
        //System.out.println(zeze.getX());
        if (zezePicture.getX() < 965) {
            if (zezePicture.getWidth() == -29) {
                zezePicture.grow(29, 0);
                zezePicture.translate(10, 0);
            }
            zezePicture.translate(20, 0);
        }
    }


    public void moveLeft() {
        //System.out.println(zeze.getX());
        if (zezePicture.getX() >= 50) {

            if (zezePicture.getWidth() != -29){ //change zeze direction
                zezePicture.grow(-29,0);
                zezePicture.translate(-10,0);
            }
            zezePicture.translate(-20, 0); //move
        }
    }

    public void moveUp() {
        //System.out.println(zeze.getY());
        if (zezePicture.getY() >= 30) {
            zezePicture.translate(0, -20);
        }
    }

    public void moveDown() {

        if (zezePicture.getY() < 620) {
            //System.out.println(zeze.getY());
            zezePicture.translate(0, 20);
        }
    }


    @Override
    public void keyPressed(KeyboardEvent keyboardEvent) {
        int keyPressed = keyboardEvent.getKey();
        if(keyPressed == 39) {
            moveRight();
        }
        if(keyPressed == 37) {
            moveLeft();
        }
        if(keyPressed == 38) {
            moveUp();
        }
        if(keyPressed == 40) {
            moveDown();
        }
    }
    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }

}
