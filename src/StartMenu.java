import org.academiadecodigo.simplegraphics.keyboard.Keyboard;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEvent;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardEventType;
import org.academiadecodigo.simplegraphics.keyboard.KeyboardHandler;
import org.academiadecodigo.simplegraphics.pictures.Picture;

public class StartMenu implements KeyboardHandler {
    private Picture background = new Picture(10, 10, "MENU.jpg");
    private Keyboard keyboard;
    private boolean isMenuActive = true;

    private Field field;


    public StartMenu() {
        this.keyboard = new Keyboard(this);
       // this.field = field;
        keyboardHandler();
    }

    public void StartMenuInit() {
        background.draw();

        try {
            while (isMenuActive) {
                Thread.sleep(30);
            }
            background.delete();
            Field field = new Field();
            field.gameInit();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void keyboardHandler() {
        KeyboardEvent startGame = new KeyboardEvent();
        startGame.setKey(KeyboardEvent.KEY_SPACE);
        startGame.setKeyboardEventType(KeyboardEventType.KEY_PRESSED);
        keyboard.addEventListener(startGame);
    }

    public boolean isMenuActive() {
        return isMenuActive;
    }

    public void setMenuActive() {
        isMenuActive = !isMenuActive;
    }

    @Override

    public void keyPressed(KeyboardEvent keyboardEvent) {
        int keyPressed = keyboardEvent.getKey();

        if (keyPressed == KeyboardEvent.KEY_SPACE) {
            isMenuActive = false;
        }
    }

    @Override
    public void keyReleased(KeyboardEvent keyboardEvent) {
    }
}