import org.academiadecodigo.simplegraphics.pictures.Picture;

public class HotBabe {

    public static final int HOTBABE_SIZE = 87;
    private Picture hotBabeShape;
    private boolean isDead;
    private Field field;
    private BabeDirection currentDirection;

    private int width = 87;

    private int height = 29;

    public HotBabe(Field field) {
        int startX = (Field.WIDTH - HOTBABE_SIZE)/2 ;
        int startY = Field.HEIGHT - 250;


        hotBabeShape = new Picture(startX, startY, "HotBabereduced.png");
        hotBabeShape.draw();
        currentDirection = BabeDirection.DOWN;

        this.field = field;
    }


    public int getX(){
        return hotBabeShape.getX();
    }

    public  int getY(){
        return hotBabeShape.getY();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public BabeDirection hotBabeMove() {
        int stepSize = 60;

        int newX = hotBabeShape.getX();
        int newY = hotBabeShape.getY();


        switch (currentDirection) {
            case UP:
                newY -= stepSize;
                break;
            case DOWN:
                newY += stepSize;
                break;
            case LEFT:
                newX -= stepSize;
                break;
            case RIGHT:
                newX += stepSize;
                break;
            case UP_LEFT:
                newX -= stepSize;
                newY -= stepSize;
                break;
            case UP_RIGHT:
                newX += stepSize;
                newY -= stepSize;
                break;
            case DOWN_LEFT:
                newX -= stepSize;
                newY += stepSize;
                break;
            case DOWN_RIGHT:
                newX += stepSize;
                newY += stepSize;
                break;

        }
        if (newX >= Field.PADDING && newX + hotBabeShape.getWidth()<= Field.WIDTH && newY >= Field.PADDING && newY + hotBabeShape.getHeight() <= Field.HEIGHT) {   // adicionamos height da babe na ultima condicao e adiconar width da babe na segunda
            hotBabeShape.translate(newX - hotBabeShape.getX(), newY - hotBabeShape.getY());
        }

        currentDirection = choseDirection();
        return currentDirection;
    }

        public BabeDirection choseDirection () {
            BabeDirection newDirection = currentDirection;

            if (Math.random() > 0.5) { // Changes direction 50% of the time
                newDirection = BabeDirection.values()[(int) (Math.random() * BabeDirection.values().length)];
            }

            return newDirection;

    }





    }



